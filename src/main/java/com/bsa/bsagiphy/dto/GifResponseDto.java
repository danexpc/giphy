package com.bsa.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GifResponseDto {
    private final String path;
}
